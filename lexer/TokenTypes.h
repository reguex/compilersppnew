//
// Created by Doninelli on 10/31/17.
//

#ifndef COMPILERS_TOKENTYPES_H
#define COMPILERS_TOKENTYPES_H

#include <string>

namespace lexer {
    class TokenType {
    public:
        static const std::string multiplication;
        static const std::string assignment;
        static const std::string equal;
        static const std::string id;
        static const std::string subtraction;
        static const std::string number;
        static const std::string addition;
        static const std::string division;
        static const std::string comment;
        static const std::string whitespace;
        static const std::string leftParentheses;
        static const std::string rightParentheses;
        static const std::string eof;
        static const std::string greaterThan;
        static const std::string lessThan;
        static const std::string semicolon;
        static const std::string greaterOrEqual;
        static const std::string lessOrEqual;
    };
}

#endif //COMPILERS_TOKENTYPES_H
