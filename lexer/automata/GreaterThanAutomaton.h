//
// Created by Doninelli on 11/13/17.
//

#ifndef COMPILERS_GREATERTHANAUTOMATON_H
#define COMPILERS_GREATERTHANAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {
        class GreaterThanAutomaton : public Automaton {
        public:
            GreaterThanAutomaton();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //COMPILERS_GREATERTHANAUTOMATON_H
