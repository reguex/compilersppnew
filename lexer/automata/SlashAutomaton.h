//
// Created by Doninelli on 10/28/17.
//

#ifndef COMPILERS_SLASHAUTOMATON_H
#define COMPILERS_SLASHAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {

        class SlashAutomaton : public Automaton {
        public:
            SlashAutomaton();

        protected:
            int delta(int current, int value) override;

            std::string getStateTokenType(int state) override;

            bool isFinal(int state) override;
        };

    }
}


#endif //COMPILERS_SLASHAUTOMATON_H
