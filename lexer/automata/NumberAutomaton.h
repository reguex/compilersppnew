//
// Created by Doninelli on 10/27/17.
//

#ifndef COMPILERS_NUMBERCONSUMER_H
#define COMPILERS_NUMBERCONSUMER_H


#include "Automaton.h"

namespace lexer {
    namespace automata {

        class NumberAutomaton : public Automaton {
        public:
            NumberAutomaton();

        protected:
            int delta(int current, int value) override;

            std::string getStateTokenType(int state) override;

            bool isFinal(int state) override;
        };

    }
}


#endif //COMPILERS_NUMBERCONSUMER_H
