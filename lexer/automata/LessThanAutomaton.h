//
// Created by Doninelli on 11/13/17.
//

#ifndef COMPILERS_LESSTHANAUTOMATON_H
#define COMPILERS_LESSTHANAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {
        class LessThanAutomaton : public Automaton {
        public:
            LessThanAutomaton();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //COMPILERS_LESSTHANAUTOMATON_H
