//
// Created by Doninelli on 11/13/17.
//

#ifndef COMPILERS_SEMICOLONAUTOMATON_H
#define COMPILERS_SEMICOLONAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {
        class SemicolonAutomaton : public Automaton {
        public:
            SemicolonAutomaton();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //COMPILERS_SEMICOLONAUTOMATON_H
