//
// Created by Edwin Herrera on 11/1/17.
//

#ifndef COMPILERS_LEFTPARENTHESESAUTOMATON_H
#define COMPILERS_LEFTPARENTHESESAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {
        class LeftParenthesesAutomaton : public Automaton {
        public:
            LeftParenthesesAutomaton();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //COMPILERS_LEFTPARENTHESESAUTOMATON_H
