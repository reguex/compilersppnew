//
// Created by Doninelli on 10/27/17.
//

#ifndef COMPILERS_IDAUTOMATON_H
#define COMPILERS_IDAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {

        class IdAutomaton : public Automaton {
        public:
            IdAutomaton();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };

    }
}


#endif //COMPILERS_IDAUTOMATON_H
