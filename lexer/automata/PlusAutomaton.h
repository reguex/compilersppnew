//
// Created by Doninelli on 10/28/17.
//

#ifndef COMPILERS_PLUSAUTOMATON_H
#define COMPILERS_PLUSAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {

        class PlusAutomaton : public Automaton {
        public:
            PlusAutomaton();

        protected:
            int delta(int current, int value) override;

            std::string getStateTokenType(int state) override;

            bool isFinal(int state) override;
        };

    }
}


#endif //COMPILERS_PLUSAUTOMATON_H
