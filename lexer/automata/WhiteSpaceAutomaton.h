//
// Created by Doninelli on 10/27/17.
//

#ifndef COMPILERS_WHITESPACEAUTOMATON_H
#define COMPILERS_WHITESPACEAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {

        class WhiteSpaceAutomaton : public Automaton {
        public:
            WhiteSpaceAutomaton();

        protected:
            int delta(int current, int value) override;

            std::string getStateTokenType(int state) override;

            bool isFinal(int state) override;
        };

    }
}


#endif //COMPILERS_WHITESPACEAUTOMATON_H
