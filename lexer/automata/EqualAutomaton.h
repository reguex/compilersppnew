//
// Created by Doninelli on 10/28/17.
//

#ifndef COMPILERS_EQUALAUTOMATON_H
#define COMPILERS_EQUALAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {

        class EqualAutomaton : public Automaton {
        public:
            EqualAutomaton();

        protected:
            std::string getStateTokenType(int state) override;

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;
        };

    }
}


#endif //COMPILERS_EQUALAUTOMATON_H
