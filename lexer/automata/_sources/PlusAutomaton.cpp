//
// Created by Doninelli on 10/28/17.
//

#include "../PlusAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::PlusAutomaton::PlusAutomaton() : Automaton(1) {}

int lexer::automata::PlusAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::plus) {
            return 2;
        }
    }

    return -1;
}

bool lexer::automata::PlusAutomaton::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::PlusAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return TokenType::addition;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}
