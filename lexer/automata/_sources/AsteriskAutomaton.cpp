//
// Created by Edwin Herrera on 10/30/17.
//

#include "../AsteriskAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::AsteriskAutomaton::AsteriskAutomaton() : Automaton(1) {}

bool lexer::automata::AsteriskAutomaton::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::AsteriskAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return TokenType::multiplication;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}

int lexer::automata::AsteriskAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::asterisk) {
            return 2;
        }
    }

    return -1;
}
