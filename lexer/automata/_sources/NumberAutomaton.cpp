//
// Created by Doninelli on 10/27/17.
//

#include "../NumberAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::NumberAutomaton::NumberAutomaton() : Automaton(1) {}

int lexer::automata::NumberAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::zero) {
            return 2;
        } else if (isnumber(value)) {
            return 7;
        }

    } else if (current == 2) {
        if (value == util::Chars::x || value == util::Chars::X) {
            return 3;
        } else if (value == util::Chars::b || value == util::Chars::B) {
            return 4;
        }

    } else if (current == 3) {
        if (ishexnumber(value)) {
            return 5;
        }

    } else if (current == 4) {
        if (util::Chars::isBinaryNumber(value)) {
            return 6;
        }

    } else if (current == 5) {
        if (ishexnumber(value)) {
            return 5;
        }

    } else if (current == 6) {
        if (util::Chars::isBinaryNumber(value)) {
            return 6;
        }

    } else if (current == 7) {
        if (isnumber(value)) {
            return 7;
        }
    }

    return -1;
}

bool lexer::automata::NumberAutomaton::isFinal(int state) {
    return state == 2 || state == 5 || state == 6 || state == 7;
}

std::string lexer::automata::NumberAutomaton::getStateTokenType(int state) {
    if (isFinal(state)) {
        return TokenType::number;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}
