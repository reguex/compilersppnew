//
// Created by Doninelli on 10/28/17.
//

#include "../SlashAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::SlashAutomaton::SlashAutomaton() : Automaton(1) {}

int lexer::automata::SlashAutomaton::delta(int current, int value) {
    switch (current) {
        case 1:
            if (value == util::Chars::slash) {
                return 2;
            }
            break;

        case 2:
            if (value == util::Chars::slash) {
                return 3;
            } else if (value == util::Chars::asterisk) {
                return 4;
            }
            break;

        case 3:
            if (value != '\n') {
                return 3;
            } else {
                return 5;
            }

        case 4:
            if (value != util::Chars::asterisk) {
                return 4;
            } else {
                return 6;
            }

        case 6:
            if (value == util::Chars::slash) {
                return 7;
            }
            break;

        default:
            break;
    }

    return -1;
}

bool lexer::automata::SlashAutomaton::isFinal(int state) {
    return state == 2 || state == 3 || state == 5 || state == 7;
}

std::string lexer::automata::SlashAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return TokenType::division;
    } else if (state == 3 || state == 5 || state == 7) {
        return TokenType::comment;
    }
}
