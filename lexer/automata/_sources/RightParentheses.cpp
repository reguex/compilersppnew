//
// Created by Edwin Herrera on 11/1/17.
//

#include "../RightParentheses.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::RightParentheses::RightParentheses() : Automaton(1) {}

int lexer::automata::RightParentheses::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::rightParentheses) {
            return 2;
        }
    }

    return -1;
}

bool lexer::automata::RightParentheses::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::RightParentheses::getStateTokenType(int state) {
    if (state == 2) {
        return lexer::TokenType::rightParentheses;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}
