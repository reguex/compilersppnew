//
// Created by Doninelli on 10/27/17.
//

#include "../Automaton.h"

lexer::automata::Automaton::Automaton(int initial) : initial(initial) {}

lexer::Token lexer::automata::Automaton::process(std::ifstream &source) {
    std::string lexeme;

    auto current = initial;
    while (!source.eof() && source.peek() != -1) {
        auto next = delta(current, source.peek());
        if (next < 0) {
            break;
        }

        current = next;
        lexeme += static_cast<char>(source.get());
    }

    if (!isFinal(current)) {
        throw std::invalid_argument("invalid input");
    }

    return Token(lexeme, getStateTokenType(current));
}

void lexer::automata::Automaton::raiseUndefinedTokenTypeForState(int state) {
    std::string error;
    error += std::to_string(state);
    throw std::invalid_argument(error);
}

lexer::automata::Automaton::~Automaton() = default;
