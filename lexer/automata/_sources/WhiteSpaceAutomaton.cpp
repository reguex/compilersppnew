//
// Created by Doninelli on 10/27/17.
//

#include "../WhiteSpaceAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::WhiteSpaceAutomaton::WhiteSpaceAutomaton() : Automaton(1) {}

int lexer::automata::WhiteSpaceAutomaton::delta(int current, int value) {
    if (current == 1 && util::Chars::isWhiteSpace(value)) {
        return 2;
    } else if (current == 2 && util::Chars::isWhiteSpace(value)) {
        return 2;
    } else {
        return -1;
    }
}

bool lexer::automata::WhiteSpaceAutomaton::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::WhiteSpaceAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return TokenType::whitespace;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}


