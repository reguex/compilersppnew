//
// Created by Doninelli on 10/28/17.
//

#include "../EqualAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::EqualAutomaton::EqualAutomaton() : Automaton(1) {}

int lexer::automata::EqualAutomaton::delta(int current, int value) {
    switch (current) {
        case 1:
            if (value == util::Chars::equal) {
                return 2;
            }
            break;

        case 2:
            if (value == util::Chars::equal) {
                return 3;
            }
            break;

        default:
            break;
    }

    return -1;
}

bool lexer::automata::EqualAutomaton::isFinal(int state) {
    return state == 2 || state == 3;
}

std::string lexer::automata::EqualAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return TokenType::assignment;
    } else if (state == 3) {
        return TokenType::equal;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}
