//
// Created by Edwin Herrera on 11/1/17.
//

#include "../LeftParenthesesAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::LeftParenthesesAutomaton::LeftParenthesesAutomaton() : Automaton(1) {}

int lexer::automata::LeftParenthesesAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::leftParentheses) {
            return 2;
        }
    }

    return -1;
}

bool lexer::automata::LeftParenthesesAutomaton::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::LeftParenthesesAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return lexer::TokenType::leftParentheses;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}
