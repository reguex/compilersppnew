//
// Created by Doninelli on 10/28/17.
//

#include "../MinusAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::MinusAutomaton::MinusAutomaton() : Automaton(1) {}

int lexer::automata::MinusAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::minus) {
            return 2;
        }
    }

    return -1;
}

bool lexer::automata::MinusAutomaton::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::MinusAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return TokenType::subtraction;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}
