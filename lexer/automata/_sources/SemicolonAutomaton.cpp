//
// Created by Doninelli on 11/13/17.
//

#include "../SemicolonAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::SemicolonAutomaton::SemicolonAutomaton() : Automaton(1) {}

int lexer::automata::SemicolonAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::semicolon) {
            return 2;
        }
    }

    return -1;
}

bool lexer::automata::SemicolonAutomaton::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::SemicolonAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return lexer::TokenType::semicolon;
    }

    raiseUndefinedTokenTypeForState(state);
}
