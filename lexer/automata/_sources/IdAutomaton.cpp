//
// Created by Doninelli on 10/27/17.
//

#include "../IdAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::IdAutomaton::IdAutomaton() : Automaton(1) {}

int lexer::automata::IdAutomaton::delta(int current, int value) {
    switch (current) {
        case 1:
            if (util::Chars::isLetter(value) || value == util::Chars::underscore) {
                return 2;
            }
            break;

        case 2:
            if (util::Chars::isLetter(value) || value == util::Chars::underscore || isnumber(value)) {
                return 2;
            }
            break;

        default:
            break;
    }

    return -1;
}

bool lexer::automata::IdAutomaton::isFinal(int state) {
    return state == 2;
}

std::string lexer::automata::IdAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return TokenType::id;
    } else {
        raiseUndefinedTokenTypeForState(state);
    }
}
