//
// Created by Doninelli on 11/13/17.
//

#include "../GreaterThanAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::GreaterThanAutomaton::GreaterThanAutomaton() : Automaton(1) {}

int lexer::automata::GreaterThanAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::greaterThan) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::equal) {
            return 3;
        }
    }

    return -1;
}

bool lexer::automata::GreaterThanAutomaton::isFinal(int state) {
    return state == 2 || state == 3;
}

std::string lexer::automata::GreaterThanAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return lexer::TokenType::greaterThan;
    } else if (state == 3) {
        return lexer::TokenType::greaterOrEqual;
    }

    raiseUndefinedTokenTypeForState(2);
}
