//
// Created by Doninelli on 11/13/17.
//

#include "../LessThanAutomaton.h"
#include "../../util/Chars.h"
#include "../../TokenTypes.h"

lexer::automata::LessThanAutomaton::LessThanAutomaton() : Automaton(1) {}

int lexer::automata::LessThanAutomaton::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::lessThan) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::equal) {
            return 3;
        }
    }

    return -1;
}

bool lexer::automata::LessThanAutomaton::isFinal(int state) {
    return state == 2 || state == 3;
}

std::string lexer::automata::LessThanAutomaton::getStateTokenType(int state) {
    if (state == 2) {
        return lexer::TokenType::lessThan;
    } else if (state == 3) {
        return lexer::TokenType::lessOrEqual;
    }

    raiseUndefinedTokenTypeForState(2);
}
