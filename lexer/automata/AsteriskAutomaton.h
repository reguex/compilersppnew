//
// Created by Edwin Herrera on 10/30/17.
//

#ifndef COMPILERS_ASTERISKAUTOMATON_H
#define COMPILERS_ASTERISKAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {
        class AsteriskAutomaton : public Automaton {
        public:
            explicit AsteriskAutomaton();

        protected:
            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;

            int delta(int current, int value) override;
        };
    }
}


#endif //COMPILERS_ASTERISKAUTOMATON_H
