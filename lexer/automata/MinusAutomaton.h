//
// Created by Doninelli on 10/28/17.
//

#ifndef COMPILERS_MINUSAUTOMATON_H
#define COMPILERS_MINUSAUTOMATON_H


#include "Automaton.h"

namespace lexer {
    namespace automata {

        class MinusAutomaton : public Automaton {
        public:
            MinusAutomaton();

        protected:
            int delta(int current, int value) override;

            std::string getStateTokenType(int state) override;

            bool isFinal(int state) override;
        };

    }
}


#endif //COMPILERS_MINUSAUTOMATON_H
