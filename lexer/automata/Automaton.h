//
// Created by Doninelli on 10/27/17.
//

#ifndef COMPILERS_AUTOMATON_H
#define COMPILERS_AUTOMATON_H


#include <fstream>
#include <vector>
#include "../Token.h"

namespace lexer {
    namespace automata {

        class Automaton {
            int initial;
        public:
            explicit Automaton(int initial);

            Token process(std::ifstream &source);

        protected:
            virtual int delta(int current, int value) = 0;
            virtual bool isFinal(int state) = 0;
            virtual std::string getStateTokenType(int state) = 0;

        public:
            virtual ~Automaton();

        protected:
            void raiseUndefinedTokenTypeForState(int state);
        };

    }
}


#endif //COMPILERS_AUTOMATON_H
