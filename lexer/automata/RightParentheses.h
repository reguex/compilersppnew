//
// Created by Edwin Herrera on 11/1/17.
//

#ifndef COMPILERS_RIGHTPARENTHESES_H
#define COMPILERS_RIGHTPARENTHESES_H


#include "Automaton.h"

namespace lexer {
    namespace automata {
        class RightParentheses : public Automaton {
        public:
            RightParentheses();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;

        };
    }
}


#endif //COMPILERS_RIGHTPARENTHESES_H
