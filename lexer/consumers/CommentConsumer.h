//
// Created by Doninelli on 10/23/17.
//

#ifndef COMPILERS_COMMENTCONSUMER_H
#define COMPILERS_COMMENTCONSUMER_H


#include "BaseConsumer.h"

namespace lexer {
    namespace consumers {
        class CommentConsumer : public BaseConsumer {
        public:
            bool isStartingPoint(int c) override;

            Token consume(std::ifstream &stream) override;
        };
    }
}


#endif //COMPILERS_COMMENTCONSUMER_H
