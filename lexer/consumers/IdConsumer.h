//
// Created by Doninelli on 10/21/17.
//

#ifndef COMPILERS_IDCONSUMER_H
#define COMPILERS_IDCONSUMER_H

#include "BaseConsumer.h"
#include <fstream>

namespace lexer {
    namespace consumers {

        class IdConsumer : public BaseConsumer {
        public:
            bool isStartingPoint(int c) override;

            Token consume(std::ifstream &stream) override;
        };
    }
}


#endif //COMPILERS_IDCONSUMER_H
