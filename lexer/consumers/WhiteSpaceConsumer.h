//
// Created by Doninelli on 10/20/17.
//

#ifndef COMPILERS_WHITESPACECONSUMER_H
#define COMPILERS_WHITESPACECONSUMER_H


#include "BaseConsumer.h"

namespace lexer {
    namespace consumers {
        class WhiteSpaceConsumer : public BaseConsumer {
        public:
            bool isStartingPoint(int c) override;

            Token consume(std::ifstream &stream) override;
        };
    }
}


#endif //COMPILERS_WHITESPACECONSUMER_H
