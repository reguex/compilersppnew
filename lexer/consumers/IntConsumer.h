//
// Created by Doninelli on 10/20/17.
//

#ifndef COMPILERS_INTCONSUMER_H
#define COMPILERS_INTCONSUMER_H


#include "BaseConsumer.h"

namespace lexer {
    namespace consumers {

        class IntConsumer : public BaseConsumer {
        public:
            bool isStartingPoint(int c) override;

            Token consume(std::ifstream &stream) override;
        };

    }
}


#endif //COMPILERS_INTCONSUMER_H
