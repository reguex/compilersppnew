//
// Created by Doninelli on 10/20/17.
//

#include "../IntConsumer.h"
#include "../../util/Chars.h"

bool lexer::consumers::IntConsumer::isStartingPoint(int c) {
    return isnumber(c);
}

lexer::Token lexer::consumers::IntConsumer::consume(std::ifstream &stream) {
    if (!isStartingPoint(stream.peek())) {
        raiseInvalidInputError(stream.peek());
    }

    std::string lexeme;

    if (stream.peek() == util::Chars::zero) {
        concat(lexeme, stream.get());

        if (stream.peek() == util::Chars::x || stream.peek() == util::Chars::X) {
            concat(lexeme, stream.get());

            while (ishexnumber(stream.peek())) {
                concat(lexeme, stream.get());
            }
        } else if (stream.peek() == util::Chars::b || stream.peek() == util::Chars::B) {
            concat(lexeme, stream.get());

            while (util::Chars::isBinaryNumber(stream.peek())) {
                concat(lexeme, stream.get());
            }
        }

        if (lexeme.length() == 2) {
            raiseInvalidInputError(stream.peek());
        }
    } else {
        bool isDecimal = true;
        while (ishexnumber(stream.peek())) {
            auto currentChar = stream.get();
            concat(lexeme, currentChar);

            if (!isnumber(currentChar) && isDecimal) {
                isDecimal = false;
            }
        }

        if (stream.peek() == util::Chars::h || stream.peek() == util::Chars::H) {
            concat(lexeme, stream.get());
        } else if (!isDecimal) {
            throw std::invalid_argument("'h' or 'H' expected");
        }
    }

    return Token(lexeme, "INTEGER");
}
