//
// Created by Doninelli on 10/21/17.
//

#include "../IdConsumer.h"
#include "../../util/Chars.h"

bool lexer::consumers::IdConsumer::isStartingPoint(int c) {
    return util::Chars::isLetter(c) || c == util::Chars::underscore;
}

lexer::Token lexer::consumers::IdConsumer::consume(std::ifstream &stream) {
    if (!isStartingPoint(stream.peek())) {
        raiseInvalidInputError(stream.peek());
    }

    std::string lexeme;
    concat(lexeme, stream.get());
    while (util::Chars::isLetter(stream.peek()) || isnumber(stream.peek()) || stream.peek() == util::Chars::underscore) {
        concat(lexeme, stream.get());
    }

    return Token(lexeme, "ID");
}