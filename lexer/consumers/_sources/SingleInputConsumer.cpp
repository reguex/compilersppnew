//
// Created by Doninelli on 10/21/17.
//

#include "../SingleInputConsumer.h"
#include "../../util/Chars.h"

bool lexer::consumers::SingleInputConsumer::isStartingPoint(int c) {
    return c == util::Chars::plus || c == util::Chars::minus || c == util::Chars::leftParentheses ||
           c == util::Chars::rightParentheses || c == util::Chars::equal;
}

lexer::Token lexer::consumers::SingleInputConsumer::consume(std::ifstream &stream) {
    if (!isStartingPoint(stream.peek())) {
        raiseInvalidInputError(stream.peek());
    }

    auto symbol = stream.get();
    auto tokenType = getAppropriateTokenType(symbol);

    std::string lexeme;
    concat(lexeme, symbol);

    return Token(lexeme, tokenType);
}

const std::string lexer::consumers::SingleInputConsumer::getAppropriateTokenType(int c) {
    switch (c) {
        case util::Chars::plus:
            return "PLUS";
        case util::Chars::minus:
            return "MINUS";
        case util::Chars::leftParentheses:
            return "LEFT_PARENTHESES";
        case util::Chars::rightParentheses:
            return "RIGHT_PARENTHESES";
        case util::Chars::equal:
            return "ASSIGNMENT";
        default:
            raiseInvalidInputError(c);
    }
}
