//
// Created by Doninelli on 10/23/17.
//

#include "../CommentConsumer.h"
#include "../../util/Chars.h"

bool lexer::consumers::CommentConsumer::isStartingPoint(int c) {
    return c == util::Chars::slash;
}

lexer::Token lexer::consumers::CommentConsumer::consume(std::ifstream &stream) {
    if (!isStartingPoint(stream.peek())) {
        raiseInvalidInputError(stream.peek());
    }

    std::string lexeme;
    concat(lexeme, stream.get());

    if (stream.peek() == util::Chars::slash) {
        concat(lexeme, stream.get());

        while(stream.peek() != '\n') {
            concat(lexeme, stream.get());
        }
    } else if (stream.peek() == util::Chars::asterisk) {
        concat(lexeme, stream.get());

        int previousInput = -1;
        while(previousInput != util::Chars::asterisk && stream.peek() != util::Chars::slash) {
            previousInput = stream.get();
            concat(lexeme, previousInput);
        }
        concat(lexeme, stream.get());
    } else {
        raiseInvalidInputError(stream.peek());
    }

    return Token(lexeme, "COMMENT");
}
