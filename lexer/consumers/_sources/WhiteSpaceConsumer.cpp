//
// Created by Doninelli on 10/20/17.
//

#include "../WhiteSpaceConsumer.h"
#include "../../util/Chars.h"

bool lexer::consumers::WhiteSpaceConsumer::isStartingPoint(int c) {
    return util::Chars::isWhiteSpace(c);
}

lexer::Token lexer::consumers::WhiteSpaceConsumer::consume(std::ifstream &stream) {
    if (!isStartingPoint(stream.peek())) {
        raiseInvalidInputError(stream.peek());
    }

    std::string lexeme;
    while (util::Chars::isWhiteSpace(stream.peek())) {
        concat(lexeme, stream.get());
    }

    return Token(lexeme, "WHITESPACE");
}
