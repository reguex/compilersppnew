//
// Created by Doninelli on 10/21/17.
//

#include "../BaseConsumer.h"

void lexer::consumers::BaseConsumer::concat(std::string &str, int i) const {
    str += static_cast<char>(i);
}

void lexer::consumers::BaseConsumer::raiseInvalidInputError(int i) const {
    std::string invalidInput;
    concat(invalidInput, i);
    throw std::invalid_argument(invalidInput);
}