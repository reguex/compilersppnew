//
// Created by Doninelli on 10/20/17.
//

#ifndef COMPILERS_BASECONSUMER_H
#define COMPILERS_BASECONSUMER_H


#include <string>
#include <fstream>
#include "../Token.h"

namespace lexer {
    namespace consumers {

        class BaseConsumer {
        protected:
            void concat(std::string &str, int i) const;

            void raiseInvalidInputError(int i) const ;

        public:
            virtual bool isStartingPoint(int c)= 0;

            virtual Token consume(std::ifstream &stream)= 0;
        };
    }
}


#endif //COMPILERS_BASECONSUMER_H
