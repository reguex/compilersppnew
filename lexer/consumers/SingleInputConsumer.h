//
// Created by Doninelli on 10/21/17.
//

#ifndef COMPILERS_SINGLEINPUTCONSUMER_H
#define COMPILERS_SINGLEINPUTCONSUMER_H


#include "BaseConsumer.h"


namespace lexer {
    namespace consumers {

        class SingleInputConsumer : public BaseConsumer {
        private:
            const std::string getAppropriateTokenType(int c);
        public:
            bool isStartingPoint(int c) override;

            Token consume(std::ifstream &stream) override;
        };
    }
}


#endif //COMPILERS_SINGLEINPUTCONSUMER_H
