#include "../TokenTypes.h"

const std::string lexer::TokenType::multiplication = "MULTIPLICATION";
const std::string lexer::TokenType::assignment = "ASSIGNMENT";
const std::string lexer::TokenType::equal = "EQUAL";
const std::string lexer::TokenType::id = "ID";
const std::string lexer::TokenType::subtraction = "SUBTRACTION";
const std::string lexer::TokenType::number = "NUMBER";
const std::string lexer::TokenType::addition = "ADDITION";
const std::string lexer::TokenType::division = "DIVISION";
const std::string lexer::TokenType::comment = "COMMENT";
const std::string lexer::TokenType::whitespace = "WHITESPACE";
const std::string lexer::TokenType::leftParentheses = "LEFT PARENTHESES";
const std::string lexer::TokenType::rightParentheses = "RIGHT PARENTHESES";
const std::string lexer::TokenType::eof = "EOF";
const std::string lexer::TokenType::greaterThan = "GREATER THAN";
const std::string lexer::TokenType::lessThan = "LESS THAN";
const std::string lexer::TokenType::semicolon = "SEMICOLON";
const std::string lexer::TokenType::greaterOrEqual = "GREATER THAN OR EQUAL";
const std::string lexer::TokenType::lessOrEqual = "LESS THAN OR EQUAL";
