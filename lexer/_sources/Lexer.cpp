//
// Created by Doninelli on 10/20/17.
//

#include "../Lexer.h"
#include "../util/Chars.h"
#include "../automata/WhiteSpaceAutomaton.h"
#include "../automata/SlashAutomaton.h"
#include "../automata/NumberAutomaton.h"
#include "../automata/IdAutomaton.h"
#include "../automata/PlusAutomaton.h"
#include "../automata/MinusAutomaton.h"
#include "../automata/EqualAutomaton.h"
#include "../automata/AsteriskAutomaton.h"
#include "../TokenTypes.h"
#include "../automata/LeftParenthesesAutomaton.h"
#include "../automata/RightParentheses.h"
#include "../automata/LessThanAutomaton.h"
#include "../automata/GreaterThanAutomaton.h"
#include "../automata/SemicolonAutomaton.h"

lexer::Lexer::Lexer(std::ifstream &input) : input(input) {}

lexer::Token lexer::Lexer::getNextToken() {
    if (input.peek() == -1 || input.eof()) {
        return Token("", TokenType::eof);
    }

    if (util::Chars::isWhiteSpace(input.peek())) {
        automata::WhiteSpaceAutomaton().process(input);
        return getNextToken();
    } else if (input.peek() == util::Chars::slash) {
        Token token = automata::SlashAutomaton().process(input);
        if (token.tokenType == TokenType::division) {
            return token;
        } else {
            return getNextToken();
        }
    }

    automata::Automaton *automaton = getAppropriateAutomaton();
    Token token = automaton->process(input);
    delete automaton;

    return token;
}

lexer::automata::Automaton *lexer::Lexer::getAppropriateAutomaton() {
    auto currentChar = input.peek();

    if (isnumber(currentChar)) {
        return new automata::NumberAutomaton();
    }

    if (currentChar == util::Chars::underscore || util::Chars::isLetter(currentChar)) {
        return new automata::IdAutomaton();
    }

    if (currentChar == util::Chars::plus) {
        return new automata::PlusAutomaton();
    }

    if (currentChar == util::Chars::minus) {
        return new automata::MinusAutomaton();
    }

    if (currentChar == util::Chars::equal) {
        return new automata::EqualAutomaton();
    }

    if (currentChar == util::Chars::asterisk) {
        return new automata::AsteriskAutomaton();
    }

    if (currentChar == util::Chars::slash) {
        return new automata::SlashAutomaton();
    }

    if (currentChar == util::Chars::leftParentheses) {
        return new automata::LeftParenthesesAutomaton();
    }

    if (currentChar == util::Chars::rightParentheses) {
        return new automata::RightParentheses();
    }

    if (currentChar == util::Chars::lessThan) {
        return new automata::LessThanAutomaton();
    }

    if (currentChar == util::Chars::greaterThan) {
        return new automata::GreaterThanAutomaton();
    }

    if (currentChar == util::Chars::semicolon) {
        return new automata::SemicolonAutomaton();
    }

    std::string error;
    error += static_cast<char>(currentChar);
    throw std::invalid_argument(error);
}


