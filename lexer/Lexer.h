//
// Created by Doninelli on 10/20/17.
//

#ifndef COMPILERS_LEXER_H
#define COMPILERS_LEXER_H


#include <fstream>
#include "Token.h"
#include "automata/Automaton.h"


namespace lexer {

    class Lexer {
        std::ifstream &input;

        automata::Automaton *getAppropriateAutomaton();

    public:
        explicit Lexer(std::ifstream &input);

        Token getNextToken();
    };
}


#endif //COMPILERS_LEXER_H
