//
// Created by Doninelli on 11/2/17.
//

#ifndef COMPILERS_SYNTAXPARSER_H
#define COMPILERS_SYNTAXPARSER_H

/**
  First:
    +	    +
    -	    -
    *	    *
    /	    /
    number	number
    id	    id
    (	    (
    )	    )
    Ep	    +, -
    Tp	    *, /
    F	    number, id, (
    T	    number, id, (
    E	    number, id, (
 */

/**
  Next:
    E	    $, )
    Ep	    $, )
    T	    +, -, $, )
    Tp	    +, -, $, )
    F	    *, /, +, -, $, )
 */

/**
    E  → T Ep       number, id, (
    Ep → + T Ep	    +
    Ep → - T Ep	    -
    T  → F Tp       number, id, (
    Tp → * F Tp	    *
    Tp → / F Tp	    /
    F  → number	    number
    F  → id	        id
    F  → ( E )	    (
 */

#include <map>
#include "../../lexer/Token.h"
#include "../../lexer/Lexer.h"
#include "../../lexer/TokenTypes.h"

namespace parser {
    namespace tables {
        class SyntaxParser {
            lexer::Lexer lexer;

        public:
            explicit SyntaxParser(const lexer::Lexer &lexer);
        };
    }
}


#endif //COMPILERS_SYNTAXPARSER_H
