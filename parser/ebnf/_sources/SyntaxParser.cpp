//
// Created by Doninelli on 11/9/17.
//

#include "../SyntaxParser.h"
#include "../../../lexer/TokenTypes.h"

parser::ebnf::SyntaxParser::SyntaxParser(const lexer::Lexer &lexer) : lexer(lexer) {}

void parser::ebnf::SyntaxParser::parse() {
    currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
    topLevel();
}

void parser::ebnf::SyntaxParser::consume(std::initializer_list<std::string> tokenTypes) {
    for (const auto &tokenType : tokenTypes) {
        if (currentToken->getTokenType() == tokenType) {
            currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
            return;
        }
    }

    throw std::invalid_argument(currentToken->getTokenType());
}

void parser::ebnf::SyntaxParser::E() {
    T();
    while (currentToken->getTokenType() == lexer::TokenType::addition ||
           currentToken->getTokenType() == lexer::TokenType::subtraction) {

        consume({lexer::TokenType::addition, lexer::TokenType::subtraction});
        T();
    }
}

void parser::ebnf::SyntaxParser::T() {
    F();
    while (currentToken->getTokenType() == lexer::TokenType::multiplication ||
           currentToken->getTokenType() == lexer::TokenType::division) {

        consume({lexer::TokenType::multiplication, lexer::TokenType::division});
        F();
    }
}

void parser::ebnf::SyntaxParser::F() {
    if (currentToken->getTokenType() == lexer::TokenType::leftParentheses) {
        consume({lexer::TokenType::leftParentheses});
        E();
        consume({lexer::TokenType::rightParentheses});
    } else {
        consume({lexer::TokenType::id, lexer::TokenType::number});
    }
}

void parser::ebnf::SyntaxParser::assign() {
    consume({lexer::TokenType::id});
    consume({lexer::TokenType::assignment});
    comparison();
}

void parser::ebnf::SyntaxParser::topLevel() {
    if (currentToken->getTokenType() == lexer::TokenType::id && currentToken->getLexeme() == "print") {
        printStatement();
        consume({lexer::TokenType::semicolon});
    } else if (currentToken->getTokenType() == lexer::TokenType::id) {
        assign();
        consume({lexer::TokenType::semicolon});
    }

    while (currentToken->getTokenType() == lexer::TokenType::id && currentToken->getLexeme() == "print" ||
           currentToken->getTokenType() == lexer::TokenType::id) {

        topLevel();
    }
}

void parser::ebnf::SyntaxParser::printStatement() {
    consume({lexer::TokenType::id});
    comparison();
}

void parser::ebnf::SyntaxParser::comparison() {
    E();
    if (currentToken->getTokenType() == lexer::TokenType::lessThan) {
        consume({lexer::TokenType::lessThan});
        E();
    } else if (currentToken->getTokenType() == lexer::TokenType::greaterThan) {
        consume({lexer::TokenType::greaterThan});
        E();
    } else {
        return;
    }
}
