//
// Created by Doninelli on 11/9/17.
//

#ifndef COMPILERS_SYNTAXPARSER_H
#define COMPILERS_SYNTAXPARSER_H


#include "../../lexer/Lexer.h"
#include <memory>
#include <initializer_list>

namespace parser {
    namespace ebnf {
        class SyntaxParser {
            lexer::Lexer lexer;
            std::unique_ptr<lexer::Token> currentToken;

        public:
            explicit SyntaxParser(const lexer::Lexer &lexer);

            void parse();

        private:
            void consume(std::initializer_list<std::string> tokenTypes);

            void E();

            void T();

            void F();

            void assign();

            void printStatement();

            void topLevel();

            void comparison();
        };
    }
}


#endif //COMPILERS_SYNTAXPARSER_H
