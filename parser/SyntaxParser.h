//
// Created by Edwin Herrera on 10/30/17.
//

#ifndef COMPILERS_SYNTAXPARSER_H
#define COMPILERS_SYNTAXPARSER_H


#include "../lexer/Lexer.h"

namespace parser {
    class SyntaxParser {
        lexer::Lexer lexer;
        std::unique_ptr<lexer::Token> currentToken;

    private:
        void E();
        void T();
        void Ep();
        void F();
        void Tp();
        
    public:
        explicit SyntaxParser(const lexer::Lexer &lexer);

        void parse();
    };
}


#endif //COMPILERS_SYNTAXPARSER_H
