//
// Created by Edwin Herrera on 10/30/17.
//

#include "../SyntaxParser.h"
#include "../../lexer/TokenTypes.h"

parser::SyntaxParser::SyntaxParser(const lexer::Lexer &lexer) : lexer(lexer), currentToken(nullptr) {}

void parser::SyntaxParser::parse() {
    currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
    E();
}

void parser::SyntaxParser::E() {
    T();
    Ep();
}

void parser::SyntaxParser::T() {
    F();
    Tp();
}

void parser::SyntaxParser::Ep() {
    if (currentToken->tokenType == lexer::TokenType::addition) {
        currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
        T();
        Ep();
    } else if (currentToken->tokenType == lexer::TokenType::subtraction) {
        currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
        T();
        Ep();
    } else {
        return;
    }
}

void parser::SyntaxParser::Tp() {
    if (currentToken->tokenType == lexer::TokenType::multiplication) {
        currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
        F();
        Tp();
    } else if (currentToken->tokenType == lexer::TokenType::division) {
        currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
        F();
        Tp();
    } else {
        return;
    }
}

void parser::SyntaxParser::F() {
    if (currentToken->tokenType == lexer::TokenType::number || currentToken->tokenType == lexer::TokenType::id) {
        currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
    } else if (currentToken->tokenType == lexer::TokenType::leftParentheses) {
        E();
        currentToken = std::make_unique<lexer::Token>(lexer.getNextToken());
        if (currentToken->tokenType != lexer::TokenType::rightParentheses) {
            std::cerr << "invalid token. Expected ')' found " << currentToken->getTokenType() << std::endl;
            exit(-1);
        }
    } else {
        std::cerr << "invalid token. Expected '(', number or id found " << currentToken->getTokenType() << std::endl;
        exit(-1);
    }
}