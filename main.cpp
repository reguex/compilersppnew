#include <iostream>
#include "lexer/Lexer.h"
#include "lexer/automata/NumberAutomaton.h"
#include "lexer/automata/SlashAutomaton.h"
#include "parser/ebnf/SyntaxParser.h"

int main() {
    std::string file;
    std::cout << "File: ";
    std::cin >> file;

    std::ifstream ifs;

    ifs.open(file);
    lexer::Lexer lexer(ifs);
    parser::ebnf::SyntaxParser parser(lexer);
    parser.parse();
    ifs.close();
}